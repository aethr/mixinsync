
class AudioEngine {
  /**
   *
   */
  store;
  context;

  constructor (store) {
    this.store = store;
    this.context = new AudioContext();
  }

  getContext () {
    return this.context;
  }
}

export default AudioEngine;
