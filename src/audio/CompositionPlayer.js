
import { Events } from '@/constants';

class CompositionPlayer {
  audio;
  composition;
  playbackQueue;
  now;
  last;
  activeSounds;

  /**
   * Create a new CompositionPlayer to play the passed composition;
   * @param composition Object
   * @param audioEngine AudioEngine
   */
  constructor (composition, audioEngine) {
    // The composition being played!
    this.composition = composition;

    this.audio = audioEngine;

    // Cache a variable to store the current time, we'll use this heavily
    this.now = 0;

    // Keep track of the timestamp of the last event(s) to be actioned
    this.last = 0;

    this.activeSounds = {};

    // Playback queue keeps track of the next composition events
    this.resetPlaybackQueue();
  }

  play () {
    this.loop();
  }

  loop () {
    // Schedule the next loop as soon as possible
    this.animationFrameRequest = requestAnimationFrame(this.loop.bind(this));

    // Keep track of the current tick, offset from when playback was started
    this.now = Date.now() - this.composition.start;

    this.resetPlaybackQueue();

    // Any events to action
    let events = this.eventsToAction();
    for (let i = 0; i < events.length; i++) {
      // Each timestamp could have multiple events, so action them all
      for (let j = 0; j < events[i].length; j++) {
        this.actionEvent(events[i][j]);
      }
    }

    // Update the last processed timestamp
    this.last = this.now;
  }

  /**
   * The playback queue keeps track of the next "events" in the composition,
   * which are stored using their scheduled time as a key. We can filter out
   * events that have been actioned which we keep track of using the timestamp
   * of the "last" event to be actioned. We also keep the queue sorted so that
   * the "next" events should be first in the queue.
   */
  resetPlaybackQueue () {
    // Make sure we're up to date
    this.playbackQueue = Object.keys(this.composition.events)
      .filter((eventTime) => eventTime >= this.last)
      .sort();
  }

  /**
   * Find the list of composition events which haven't been actioned, but have
   * a scheduled timestamp in the past.
   *
   * @returns Array
   */
  eventsToAction () {
    return this.playbackQueue.filter((eventTime) => eventTime > this.last && eventTime <= this.now)
      .map((eventKey) => this.composition.events[eventKey]);
  }

  actionEvent (playbackEvent) {
    console.log('playbackEvent', playbackEvent);
    switch (playbackEvent.type) {
      case Events.OSCILLATOR_START:
        this.startSound(playbackEvent.frequency, playbackEvent);
        break;

      case Events.OSCILLATOR_END:
        this.stopSound(playbackEvent.frequency);
        break;
    }
  }

  startSound (soundId, playbackEvent) {
    let node = new OscillatorNode(this.audio.getContext(), {
      frequency: playbackEvent.frequency,
      type: 'sawtooth'
    });
    node.connect(this.audio.getContext().destination);

    this.activeSounds[soundId] = node;
    this.activeSounds[soundId].start();
  }

  stopSound (soundId) {
    if (this.activeSounds[soundId] === undefined) {
      return;
    }

    this.activeSounds[soundId].stop();
    delete(this.activeSounds[soundId]);
  }
}

export default CompositionPlayer;
