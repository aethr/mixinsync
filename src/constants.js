
let Mutations = {
  CONTROL_ACTIVATE: 'control_activate',
  CONTROL_DEACTIVATE: 'control_deactivate',

  COMPOSITION_START: 'composition_start',
  COMPOSITION_ADD_EVENT: 'composition_add_event',
};

let Instruments = {
  PIANO: 0,
};

let Sounds = {
  PIANO_40: 40,
  PIANO_41: 41,
  PIANO_42: 42,
  PIANO_43: 43,
  PIANO_44: 44,
  PIANO_45: 45,
  PIANO_46: 46,
  PIANO_47: 47,
  PIANO_48: 48,
  PIANO_49: 49,
  PIANO_50: 50,
  PIANO_51: 51,
};

let Events = {
  OSCILLATOR_START: 0,
  OSCILLATOR_END: 1,
};

export { Instruments, Sounds, Mutations, Events };
