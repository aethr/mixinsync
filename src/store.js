import Vue from 'vue'
import Vuex from 'vuex'

import { Mutations, Instruments, Sounds, Events } from '@/constants';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    performerOffset: 100,
    instruments: {
      [Instruments.PIANO]: {
        controls: {
          40: {
            id: 40,
            key: 'C',
            name: 'C4',
            sound: Sounds.PIANO_40,
            eventCode: 'KeyA',
            pressed: false,
          },
          41: {
            id: 41,
            key: 'C♯',
            name: 'C♯4',
            sound: Sounds.PIANO_41,
            eventCode: 'KeyW',
            pressed: false,
          },
          42: {
            id: 42,
            key: 'D',
            name: 'D4',
            sound: Sounds.PIANO_42,
            eventCode: 'KeyS',
            pressed: false,
          },
          43: {
            id: 43,
            key: 'D♯',
            name: 'D♯4',
            sound: Sounds.PIANO_43,
            eventCode: 'KeyE',
            pressed: false,
          },
          44: {
            id: 44,
            key: 'E',
            name: 'E4',
            sound: Sounds.PIANO_44,
            eventCode: 'KeyD',
            pressed: false,
          },
          45: {
            id: 45,
            key: 'F',
            name: 'F4',
            sound: Sounds.PIANO_45,
            eventCode: 'KeyF',
            pressed: false,
          },
          46: {
            id: 46,
            key: 'F♯',
            name: 'F♯4',
            sound: Sounds.PIANO_46,
            eventCode: 'KeyT',
            pressed: false,
          },
          47: {
            id: 47,
            key: 'G',
            name: 'G4',
            sound: Sounds.PIANO_47,
            eventCode: 'KeyG',
            pressed: false,
          },
          48: {
            id: 48,
            key: 'G♯',
            name: 'G♯4',
            sound: Sounds.PIANO_48,
            eventCode: 'KeyY',
            pressed: false,
          },
          49: {
            id: 49,
            key: 'A',
            name: 'A4',
            sound: Sounds.PIANO_49,
            eventCode: 'KeyH',
            pressed: false,
          },
          50: {
            id: 50,
            key: 'A♯',
            name: 'A♯4',
            sound: Sounds.PIANO_50,
            eventCode: 'KeyU',
            pressed: false,
          },
          51: {
            id: 51,
            key: 'B',
            name: 'B4',
            sound: Sounds.PIANO_51,
            eventCode: 'KeyJ',
            pressed: false,
          },
        }
      },
    },
    sounds: {
      [Sounds.PIANO_40]: {
        id: Sounds.PIANO_40,
        frequency: 261.6256,
      },
      [Sounds.PIANO_41]: {
        id: Sounds.PIANO_41,
        frequency: 277.1826,
      },
      [Sounds.PIANO_42]: {
        id: Sounds.PIANO_42,
        frequency: 293.6648,
      },
      [Sounds.PIANO_43]: {
        id: Sounds.PIANO_43,
        frequency: 311.1270,
      },
      [Sounds.PIANO_44]: {
        id: Sounds.PIANO_44,
        frequency: 329.6276,
      },
      [Sounds.PIANO_45]: {
        id: Sounds.PIANO_45,
        frequency: 349.2282,
      },
      [Sounds.PIANO_46]: {
        id: Sounds.PIANO_46,
        frequency: 369.9944,
      },
      [Sounds.PIANO_47]: {
        id: Sounds.PIANO_47,
        frequency: 391.9954,
      },
      [Sounds.PIANO_48]: {
        id: Sounds.PIANO_48,
        frequency: 415.3047,
      },
      [Sounds.PIANO_49]: {
        id: Sounds.PIANO_49,
        frequency: 440.0000,
      },
      [Sounds.PIANO_50]: {
        id: Sounds.PIANO_50,
        frequency: 466.1638,
      },
      [Sounds.PIANO_51]: {
        id: Sounds.PIANO_51,
        frequency: 493.8833,
      },
    },
    composition: {
      start: 0,
      events: {},
    },
  },
  getters: {
    getInstrumentControls: (state) => (instrument) => {
      return state.instruments[instrument].controls;
    }
  },
  mutations: {
    [Mutations.CONTROL_ACTIVATE] (state, payload) {
      let control = state.instruments[payload.instrument].controls[payload.control];
      control.pressed = true;

      let sound = state.sounds[control.sound];

      this.commit(Mutations.COMPOSITION_ADD_EVENT, {
        type: Events.OSCILLATOR_START,
        frequency: sound.frequency,
      });
    },
    [Mutations.CONTROL_DEACTIVATE] (state, payload) {
      let control = state.instruments[payload.instrument].controls[payload.control];
      control.pressed = false;

      let sound = state.sounds[control.sound];

      this.commit(Mutations.COMPOSITION_ADD_EVENT, {
        type: Events.OSCILLATOR_END,
        frequency: sound.frequency,
      });
    },
    [Mutations.COMPOSITION_START] (state) {
      state.composition.start = Date.now();
    },
    [Mutations.COMPOSITION_ADD_EVENT] (state, payload) {
      // Record event timestamps relative to a start time of zero to make
      // playback easier
      let timestamp = Date.now() - state.composition.start + state.performerOffset;

      // If no event exists at the specified timestamp, create a new container
      // for any events triggered at this time
      if (state.composition.events[timestamp] === undefined) {
        state.composition.events[timestamp] = [];
      }

      // Add the current event
      state.composition.events[timestamp].push(payload);
    },
  },
  actions: {

  }
})
